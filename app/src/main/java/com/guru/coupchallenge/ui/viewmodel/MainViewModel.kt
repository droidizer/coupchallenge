package com.guru.coupchallenge.ui.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.content.res.Resources
import android.databinding.Bindable
import android.databinding.ObservableArrayList
import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View
import com.blackbelt.bindings.notifications.ClickItemWrapper
import com.blackbelt.bindings.notifications.MessageWrapper
import com.blackbelt.bindings.recyclerviewbindings.AndroidItemBinder
import com.blackbelt.bindings.recyclerviewbindings.ItemClickListener
import com.blackbelt.bindings.viewmodel.BaseViewModel
import com.guru.coupchallenge.BR
import com.guru.coupchallenge.R
import com.guru.coupchallenge.api.IRepositoryManager
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposables
import java.util.concurrent.TimeUnit
import javax.inject.Inject

open class MainViewModel constructor(repositoryManager: IRepositoryManager, resources: Resources) : BaseViewModel() {

    private val mItems = ObservableArrayList<Any>()

    private var mFirstLoading: Boolean = true

    override fun onCreate() {
        super.onCreate()
        subscribeForDataChanges()
    }

    fun loadScooters(time: Long) {
        mScooterListDisposable = mRepositoryManager.getScooters()
            .map {
                it.map { mItems.add(ScooterItemViewModel(it)) }
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (mFirstLoading) {
                    setFistLoading(false)
                }
                notifyPropertyChanged(BR.scooters)
            }) { handleError(it) }
    }

    private fun setFistLoading(loading: Boolean) {
        mFirstLoading = loading
        notifyPropertyChanged(BR.firstLoading)
    }

    @Bindable
    fun getItemDecoration(): RecyclerView.ItemDecoration = mItemDecoration

    @Bindable
    fun isFirstLoading(): Boolean = mFirstLoading

    private fun subscribeForDataChanges() {
        if (mScootersDisposable.isDisposed) {
            mScootersDisposable =
                Observable
                    .interval(10, TimeUnit.SECONDS)
                    .startWith(0L)
                    .subscribe({ loadScooters(it) }) { handleError(it) }
        }
    }

    fun getItemClickListener(): ItemClickListener {
        return object : ItemClickListener {
            override fun onItemClicked(view: View, item: Any) {
                val scooterItemViewModel = item as? ScooterItemViewModel ?: return
                mItemClickNotifier.value = ClickItemWrapper.withAdditionalData(0, scooterItemViewModel.getScooter())
            }
        }
    }

    private val mResources = resources

    private val mTemplates: Map<Class<*>, AndroidItemBinder> =
        hashMapOf(
            ScooterItemViewModel::class.java to AndroidItemBinder(
                R.layout.scooter_item,
                BR.scooterItemViewModel
            )
        )

    private val mRepositoryManager = repositoryManager

    private var mScootersDisposable = Disposables.disposed()

    private var mScooterListDisposable = Disposables.disposed()

    private val mItemDecoration by lazy {
        val margin: Int = mResources.getDimension(R.dimen.margin_4).toInt()
        val lateralMargin: Int = mResources.getDimension(R.dimen.margin_16).toInt()
        object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                outRect?.bottom = margin
                outRect?.top = margin
                outRect?.left = lateralMargin
                outRect?.right = lateralMargin
            }
        }
    }

    @Bindable
    fun getTemplatesForObjects(): Map<Class<*>, AndroidItemBinder> = mTemplates

    @Bindable
    fun getScooters(): List<Any> = mItems

    fun getItemClickNotifier() = mItemClickNotifier

    override fun onCleared() {
        super.onCleared()
        mScootersDisposable.dispose()
        mScooterListDisposable.dispose()
    }

    open fun handleError(throwable: Throwable) {
        mMessageNotifier.value = MessageWrapper.withSnackBar(mResources.getString(R.string.something_went_wrong))
    }

    class Factory @Inject constructor(resources: Resources, repositoryManager: IRepositoryManager) :
        ViewModelProvider.NewInstanceFactory() {

        private val resources = resources
        private val repositoryManager = repositoryManager

        override fun <T : ViewModel?> create(modelClass: Class<T>) = MainViewModel(repositoryManager, resources) as T
    }
}