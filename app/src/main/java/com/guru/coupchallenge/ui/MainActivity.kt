package com.guru.coupchallenge.ui

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import com.blackbelt.bindings.activity.BaseBindingActivity
import com.guru.coupchallenge.BR
import com.guru.coupchallenge.R
import com.guru.coupchallenge.ui.viewmodel.MainViewModel
import dagger.android.AndroidInjection
import javax.inject.Inject

class MainActivity : BaseBindingActivity() {
    @Inject
    lateinit var mFactory: MainViewModel.Factory

    private val mMainViewmodel by lazy {
        ViewModelProviders.of(this, mFactory).get(MainViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
        setContentView(R.layout.activity_main, BR.mainViewModel, mMainViewmodel)
    }
}
