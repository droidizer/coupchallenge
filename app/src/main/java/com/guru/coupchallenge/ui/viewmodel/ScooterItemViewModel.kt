package com.guru.coupchallenge.ui.viewmodel

import com.guru.coupchallenge.api.models.Location
import com.guru.coupchallenge.api.models.Scooter

class ScooterItemViewModel(scooter: Scooter) {

    private val scooter = scooter

    fun getModel(): String? = "Model: " + scooter.model

    fun getLicensePlate(): String? = scooter.licensePlate

    fun getDistance(): String? = "Distance: " + (scooter.distanceToTravel).toString() + " km"

    fun getLocation(): Location? = scooter.location

    fun getEnergyLevel(): String? = "Battery: " + (scooter.energyLevel).toString()  + " %"

    fun getScooter(): Scooter = scooter
}
