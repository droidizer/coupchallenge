package com.guru.coupchallenge.api

import com.guru.coupchallenge.api.models.Scooter
import io.reactivex.Observable

interface IRepositoryManager {

    fun getScooters(): Observable<List<Scooter>>

}
