package com.guru.coupchallenge.api.models

import com.google.gson.annotations.SerializedName

data class Scooter(
    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("model")
    val model: String? = null,

    @field:SerializedName("license_plate")
    val licensePlate: String? = null,

    @field:SerializedName("energy_level")
    val energyLevel: Int? = 0,

    @field:SerializedName("distance_to_travel")
    val distanceToTravel: Double? = 0.00,

    @field:SerializedName("location")
    val location: Location? = null
)

data class Location(
    @field:SerializedName("lng")
    val longitude: Double? = 0.000000,

    @field:SerializedName("lat")
    val latitude: Double? = 0.000000
)