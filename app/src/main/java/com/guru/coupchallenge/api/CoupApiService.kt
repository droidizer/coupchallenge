package com.guru.coupchallenge.api

import com.guru.coupchallenge.api.models.ApiResponseBody
import io.reactivex.Observable
import retrofit2.http.GET

interface CoupApiService  {

    @GET("/prod/scooters")
    fun getScooters(): Observable<ApiResponseBody>

}