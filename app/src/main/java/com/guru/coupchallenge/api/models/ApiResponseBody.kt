package com.guru.coupchallenge.api.models

import com.google.gson.annotations.SerializedName

data class ApiResponseBody(

    @field:SerializedName("data")
    val data: DataModel? = null
)
