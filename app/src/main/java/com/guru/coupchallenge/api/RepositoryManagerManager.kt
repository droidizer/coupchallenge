package com.guru.coupchallenge.api

import com.guru.coupchallenge.api.models.ApiResponseBody
import com.guru.coupchallenge.api.models.Scooter
import io.reactivex.Observable
import io.reactivex.disposables.Disposables
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

open class RepositoryManagerManager @Inject constructor(apiService: CoupApiService) : IRepositoryManager {

    private val mApiService = apiService

    private var mDisposable = Disposables.disposed()

    override fun getScooters(): Observable<List<Scooter>> {
        val scootersSubject = BehaviorSubject.create<List<Scooter>>()

        mDisposable = mApiService.getScooters()
            .filter { model: ApiResponseBody -> model.data != null}
            .subscribe(
                { scootersSubject.onNext(it.data!!.scootersList) },
                { scootersSubject::onError },
                { scootersSubject::onComplete })
        return scootersSubject.hide()
    }
}

