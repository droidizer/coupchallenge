package com.guru.coupchallenge.api.models

import com.google.gson.annotations.SerializedName

data class DataModel(

    @field:SerializedName("scooters")
    val scootersList: List<Scooter> = ArrayList()
)
