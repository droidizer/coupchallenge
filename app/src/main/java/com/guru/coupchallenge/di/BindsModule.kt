package com.guru.coupchallenge.di

import com.guru.coupchallenge.api.RepositoryManagerManager
import com.guru.coupchallenge.api.IRepositoryManager
import dagger.Binds
import dagger.Module

@Module
abstract class BindsModule {

    @Binds
    abstract fun bindDataRepository(repositoryManager: RepositoryManagerManager): IRepositoryManager

}