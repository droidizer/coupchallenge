package com.guru.coupchallenge.di

import com.guru.coupchallenge.CoupApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = arrayOf(
        AndroidSupportInjectionModule::class,
        BindsModule::class,
        BuildersModule::class,
        NetworkModule::class, HelpersModule::class
    )
)

interface CoupComponent {
    fun inject(app: CoupApplication)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: CoupApplication): Builder

        fun build(): CoupComponent
    }
}