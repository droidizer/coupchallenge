package com.guru.coupchallenge.di

import com.guru.coupchallenge.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class BuildersModule {

    @ContributesAndroidInjector
    abstract fun bindRepositoryActivity(): MainActivity
}